﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace EmvoInterface.XmlFileGeneratorHelpers
{
	
	class General
	{
		private static Model.DataSet1TableAdapters.xPackSerialDsTableAdapter serialIdsTA = new Model.DataSet1TableAdapters.xPackSerialDsTableAdapter();

		private static Model.DataSet1.MemberStateISODataTable StateIsoTable = new Model.DataSet1TableAdapters.MemberStateISOTableAdapter().GetData();
		internal static XDeclaration DefaultXmlDeclaration => new XDeclaration("1.0", "utf-8", null);

		internal static IEnumerable<XElement> GetMarkets(Guid masterDataId)
		{
			return new Model.DataSet1TableAdapters.MarketSpecificDataTableAdapter().GetDataByMaterDataId(masterDataId).Select(mRow => new XElement("Market",
				new XElement("Id", GetISOCountry(mRow.MemberStateISOID)),
				new XElement("NationalCode", mRow.NationalCode),
				new	XElement("Article57Code", mRow.IsArticle57CodeNull() ? "" : mRow.Article57Code),
				GetManufacturer(mRow.ManufacturerID),
				new XElement("ContractedWholeSalers", GetWholeSalers(mRow.MarketID))
				));
		}

		private static IEnumerable<XElement> GetWholeSalers(Guid marketId)
		{
			var gpta = new Model.DataSet1TableAdapters.GPTableAdapter();
			return new Model.DataSet1TableAdapters.WholeSalerTableAdapter().GetDataByMarketId(marketId)
				.Select(wsRow => {
					var gpRow = gpta.GetDataByID(wsRow.GPID).First();
					return new XElement("Wholesaler",
						new XElement("Id", wsRow.WholeSalerNumber),
						new XElement("Name", gpRow.GPName),
						new XElement("Street1", gpRow.GPStreet1),
						new XElement("Street2", gpRow.GPStreet2),
						new XElement("City", gpRow.GPCity),
						new XElement("PostCode", gpRow.GPPostCode),
						new XElement("CountryCode", GetISOCountry(gpRow.MemberStateISOID))
						);
				});
		}

		internal static XElement GetManufacturer(Guid manufacturerId, string tag = "MAH")
		{
			var manuRow = new Model.DataSet1TableAdapters.ManufacturerTableAdapter().GetDataByID(manufacturerId).First();
			var xManu = new XElement(tag, new XElement("Id", manuRow.ManufacturerNumber));
			var gpRow = new Model.DataSet1TableAdapters.GPTableAdapter().GetDataByID(manuRow.GPID).First();
			if (!gpRow.IsGPNameNull()) xManu.Add(new XElement("Name", gpRow.GPName));
			if (!gpRow.IsGPStreet1Null()) xManu.Add(new XElement("Street1", gpRow.GPStreet1));
			if (!gpRow.IsGPStreet2Null()) xManu.Add(new XElement("Street2", gpRow.GPStreet2));
			if (!gpRow.IsGPCityNull()) xManu.Add(new XElement("City", gpRow.GPCity));
			if (!gpRow.IsGPPostCodeNull()) xManu.Add(new XElement("PostCode", gpRow.GPPostCode));
			if (!gpRow.IsMemberStateISOIDNull()) xManu.Add(new XElement("CountryCode", GetISOCountry(gpRow.MemberStateISOID)));
			return xManu;
		}

		internal static string GetISOCountry(int memberStateISOID)
		{
			return StateIsoTable.First(row => row.MemberStateISOID == memberStateISOID).MemberStateISO;
		}
		internal static IEnumerable<XElement> GetSerialIds(Guid packId)
		{
			return serialIdsTA.GetDataByPackId(packId).Select(sRow => new XElement("Id", sRow.SerialId));
		}
	}
}
