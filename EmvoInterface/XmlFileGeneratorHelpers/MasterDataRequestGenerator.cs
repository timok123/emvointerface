﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using EmvoInterface.Model;

namespace EmvoInterface.XmlFileGeneratorHelpers
{
	class MasterDataRequestGenerator
	{
		public static XDocument GetMasterDataFromTable()
		{
			
			return new XDocument(General.DefaultXmlDeclaration, new XElement("ArrayOfMasterDataRequest", GetMasterDataRequests()));
		}
		public static XDocument GetMasterDataFromTable(Guid sessionId)
		{
			return GetMasterDataFromTable(new Model.DataSet1TableAdapters.xMasterDataTableAdapter().GetDataBySessionId(sessionId));
		}

		public static XDocument GetMasterDataFromTable(IEnumerable<DataSet1.xMasterDataRow> masterDataRows)
		{
			return new XDocument(General.DefaultXmlDeclaration, new XElement("ArrayOfMasterDataRequest", GetMasterDataRequests(masterDataRows)));
		}

		private static IEnumerable<XElement> GetMasterDataRequests()
		{
			var masterDataTable = new Model.DataSet1TableAdapters.MasterDataTableAdapter().GetData();
			return masterDataTable.Select(mdRow => new XElement("MasterDataRequest", 
				new XElement("CodeScheme", mdRow.CodingScheme),
				new XElement("CodeValue", mdRow.ProductCode),
				new XElement("Name", mdRow.ProductName),
				new XElement("CommonName", mdRow.CommonName),
				new XElement("FormType", mdRow.PharmaceuticalForm),
				new XElement("Strength", mdRow.Strengh),
				new XElement("PackType", mdRow.PackType),
				new XElement("PackSize", mdRow.PackSize),
				new XElement("Markets", General.GetMarkets(mdRow.MasterDataID))
				));
		}
		private static IEnumerable<XElement> GetMasterDataRequests(IEnumerable<DataSet1.xMasterDataRow> masterDataRows)
		{
			return masterDataRows.Select(mdRow => new XElement("MasterDataRequest",
				new XElement("CodeScheme", mdRow.CodingScheme),
				new XElement("CodeValue", mdRow.ProductCode),
				new XElement("Name", mdRow.ProductName),
				new XElement("CommonName", mdRow.CommonName),
				new XElement("FormType", mdRow.PharmaceuticalForm),
				new XElement("Strength", mdRow.Strengh),
				new XElement("PackType", mdRow.PackType),
				new XElement("PackSize", mdRow.PackSize),
				new XElement("Markets", General.GetMarkets(mdRow.MasterDataID))
				));
		}
	}
}
