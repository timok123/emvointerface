﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using EmvoInterface.Model;

namespace EmvoInterface.XmlFileGeneratorHelpers
{
	class PackCreateRequestGenerator
	{
		
		public static XDocument GetPackCreateDataFromTable(Guid sessionId)
		{
			return GetPackCreateDataFromTable(new Model.DataSet1TableAdapters.xPackCreateTableAdapter().GetDataBySessionId(sessionId));
		}
		public static XDocument GetPackCreateDataFromTable(IEnumerable<DataSet1.xPackCreateRow> packCreateRows)
		{
			return new XDocument(General.DefaultXmlDeclaration, new XElement("ArrayOfPackCreateDistributorRequest", GetPackCreateDistributorRequests(packCreateRows)));
		}

		private static IEnumerable<XElement> GetPackCreateDistributorRequests(IEnumerable<DataSet1.xPackCreateRow> packCreateRows)
		{
			return packCreateRows.Select(row => new XElement("PackCreateDistributorRequest",
				new XElement("CodeScheme", row.CodeScheme),
				new XElement("CodeValue", row.CodeValue),
				new XElement("BatchId", row.BatchId),
				new XElement("BatchExpiry", row.BatchExpiry),
				General.GetManufacturer(row.ManufacturerID, "Manufacturer"),
				new XElement("SerialIds", General.GetSerialIds(row.xPackCreateID)),
				new XElement("OriginalCodeScheme", row.OriginalCodeScheme),
				new XElement("OriginalCodeValue", row.OriginalCodeValue),
				new XElement("OriginalBatchId", row.OriginalBatchId)
				));
		}
	}
}
