﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace EmvoInterface.XmlFileGeneratorHelpers
{
	class BatchUpdateRequestGenerator
	{
		public static XDocument GetBatchUpdateDataFromTable(Guid sessionId)
		{
			return new XDocument(General.DefaultXmlDeclaration, new XElement("ArrayOfBatchUpdateRequest", GetBatchUpdateRequests(sessionId)));
		}

		private static IEnumerable<XElement> GetBatchUpdateRequests(Guid sessionId)
		{
			throw new NotImplementedException();
		}
	}
}
