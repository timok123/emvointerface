﻿using EmvoInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace EmvoInterface.XmlFileGeneratorHelpers
{
	class PackVerificationRequestGenerator
	{
		public static XDocument GetPackVerificationDataFromTable(Guid sessionId)
		{
			return GetPackVerificationDataFromTable(new Model.DataSet1TableAdapters.xPackVerificationTableAdapter().GetDataBySessionId(sessionId));
		}

		public static XDocument GetPackVerificationDataFromTable(IEnumerable<DataSet1.xPackVerificationRow> packVerificationRows)
		{
			return new XDocument(General.DefaultXmlDeclaration, new XElement("ArrayOfPackVerificationRequest", GetPackVerificationRequests(packVerificationRows)));
		}

		private static IEnumerable<XElement> GetPackVerificationRequests(IEnumerable<DataSet1.xPackVerificationRow> packVerificationRows)
		{
			return packVerificationRows.Select(row => new XElement("PackVerificationRequest",
				new XElement("CodeScheme", row.CodeScheme),
				new XElement("CodeValue", row.CodeValue),
				new XElement("BatchId", row.BatchId),
				new XElement("BatchExpiry", row.BatchExpiry),
				new XElement("SerialIds", General.GetSerialIds(row.xPackVerificationID))
				));
		}
	}
}
