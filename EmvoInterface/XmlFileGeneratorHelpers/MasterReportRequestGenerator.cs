﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace EmvoInterface.XmlFileGeneratorHelpers
{
	class MasterReportRequestGenerator
	{
		public static XDocument GetMasterReportDataFromTable()
		{
			return new XDocument(General.DefaultXmlDeclaration, new XElement("ArrayOfMasterReportRequest", GetMasterReportRequests()));
		}

		private static IEnumerable<XElement> GetMasterReportRequests()
		{
			var masterDataTable = new Model.DataSet1TableAdapters.MasterDataTableAdapter().GetData();
			return masterDataTable.Select(row => new XElement("MasterReportRequest",
				new XElement("CodeScheme", row.CodingScheme),
				new XElement("CodeValue", row.ProductCode)));
		}
	}
}
