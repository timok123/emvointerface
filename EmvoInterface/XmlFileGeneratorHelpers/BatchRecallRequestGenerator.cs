﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace EmvoInterface.XmlFileGeneratorHelpers
{
	class BatchRecallRequestGenerator
	{
		public static XDocument GetBatchRecallDataFromTable(Guid sessionId)
		{
			return GetBatchRecallDataFromTable(new Model.DataSet1TableAdapters.xBatchRecallTableAdapter().GetDataBySessionId(sessionId));
		}
		public static XDocument GetBatchRecallDataFromTable(IEnumerable<Model.DataSet1.xBatchRecallRow> batchRecallRows)
		{
			return new XDocument(General.DefaultXmlDeclaration, new XElement("ArrayOfBatchRecallRequest", GetBatchRecallRequests(batchRecallRows)));
		}

		private static IEnumerable<XElement> GetBatchRecallRequests(IEnumerable<Model.DataSet1.xBatchRecallRow> batchRecallRows)
		{
			return batchRecallRows.Select(row => new XElement("BatchRecallRequest", 
				new XElement("CodeScheme", row.CodeScheme),
				new XElement("CodeValue", row.CodeValue),
				new XElement("BatchId", row.BatchId),
				new	XElement("BatchExpiry", row.BatchExpiry),
				new XElement("MarketIds", GetMarketIds(row.xBatchRecallID)),
				new XElement("Details", row.Details)
				));
		}
		private static IEnumerable<XElement> GetMarketIds(Guid batchRecallId)
		{
			var table = new Model.DataSet1TableAdapters.xBatchMarketIDsTableAdapter().GetDataByBatchId(batchRecallId);
			return table.Select(row => new XElement("Id", row.MarketId));
	}
	}
}
