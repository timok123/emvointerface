﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace EmvoInterface.XmlFileGeneratorHelpers
{
	class PackUpdateRequestGenerator
	{

		public static XDocument GetPackUpdateDataFromTable(Guid sessionId)
		{
			return GetPackUpdateDataFromTable(new Model.DataSet1TableAdapters.xPackUpdateTableAdapter().GetDataBySessionId(sessionId));
		}
		public static XDocument GetPackUpdateDataFromTable(IEnumerable<Model.DataSet1.xPackUpdateRow> packUpdateRows)
		{
			return new XDocument(General.DefaultXmlDeclaration, new XElement("ArrayOfPackUpdateRequest", GetPackUpdateRequests(packUpdateRows)));
		}

		private static IEnumerable<XElement> GetPackUpdateRequests(IEnumerable<Model.DataSet1.xPackUpdateRow> packUpdateRows)
		{
			return packUpdateRows.Select(row => new XElement("PackUpdateRequest",
				new XElement("CodeScheme", row.CodeScheme),
				new XElement("CodeValue", row.CodeValue),
				new XElement("BatchId", row.BatchId),
				new XElement("BatchExpiry", row.BatchExpiry),
				new XElement("CurrentStatus", row.CurrentStatus),
				new XElement("NewStatus", row.NewStatus),
				new XElement("SerialIds", General.GetSerialIds(row.xPackUpdateID))
				));
		}
	}
}
