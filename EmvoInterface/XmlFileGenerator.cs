﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace EmvoInterface
{
	/// <summary>
	/// 
	/// </summary>
    public class XmlFileGenerator
    {
		#region Master Data
		/// <summary>
		/// Deprecated: Generates a XML Document containing an "ArrayOfMasterDataReuqest" based on the Product Master Data contained in the MasterDAta table.
		/// </summary>
		/// <returns>XML Document containing the "ArrayOfMasterDataRequest"</returns>
		[Obsolete("this method is deprecated. Please use GetMasterDataRequest(Guid sessionId) instead.")]
		public static XDocument GetMasterDataRequest()
		{
			return XmlFileGeneratorHelpers.MasterDataRequestGenerator.GetMasterDataFromTable();
		}
		/// <summary>
		/// Generates a XML Document containing an "ArrayOfMasterDataRequest" based on the Product Master Data contained in the xMasterData table of the given session
		/// </summary>
		/// <param name="sessionId">the id that identifies the given session</param>
		/// <returns>XML Document containing the "ArrayOfMasterDataRequest"</returns>
		public static XDocument GetMasterDataRequest(Guid sessionId) => XmlFileGeneratorHelpers.MasterDataRequestGenerator.GetMasterDataFromTable(sessionId);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="masterDataTable"></param>
		/// <returns></returns>
		public static XDocument GetMasterDataRequest(IEnumerable<Model.DataSet1.xMasterDataRow> masterDataRows) => XmlFileGeneratorHelpers.MasterDataRequestGenerator.GetMasterDataFromTable(masterDataRows);
		#endregion

		#region Pack Verification
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sessionId"></param>
		/// <returns></returns>
		public static XDocument GetPackDataVerificationRequest(Guid sessionId)
		{
			return XmlFileGeneratorHelpers.PackVerificationRequestGenerator.GetPackVerificationDataFromTable(sessionId);
		}
		public static XDocument GetPackDataVerificationRequest(IEnumerable<Model.DataSet1.xPackVerificationRow> packVerificationRows)
		{
			return XmlFileGeneratorHelpers.PackVerificationRequestGenerator.GetPackVerificationDataFromTable(packVerificationRows);
		}
		#endregion

		#region Pack Create
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sessionId"></param>
		/// <returns></returns>
		public static XDocument GetPackCreateRequest(Guid sessionId)
		{
			return XmlFileGeneratorHelpers.PackCreateRequestGenerator.GetPackCreateDataFromTable(sessionId);
		}
		public static XDocument GetPackCreateRequest(IEnumerable<Model.DataSet1.xPackCreateRow> packCreateRows)
		{
			return XmlFileGeneratorHelpers.PackCreateRequestGenerator.GetPackCreateDataFromTable(packCreateRows);
		}
		#endregion

		#region Pack Update
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sessionId"></param>
		/// <returns></returns>
		public static XDocument GetPackUpdateRequest(Guid sessionId)
		{
			return XmlFileGeneratorHelpers.PackUpdateRequestGenerator.GetPackUpdateDataFromTable(sessionId);
		}
		public static XDocument GetPackUpdateRequest(IEnumerable<Model.DataSet1.xPackUpdateRow> packUpdateRows)
		{
			return XmlFileGeneratorHelpers.PackUpdateRequestGenerator.GetPackUpdateDataFromTable(packUpdateRows);
		}
		#endregion

		#region Batch Update
		/// <summary>
		/// not yet implemented.
		/// </summary>
		/// <param name="sessionId"></param>
		/// <returns></returns>
		[Obsolete("This method is not yet implemented", true)]
		public static XDocument GetBatchUpdateRequest(Guid sessionId)
		{
			throw new NotImplementedException();
		}
		#endregion

		#region Batch Recall
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sessionId"></param>
		/// <returns></returns>
		public static XDocument GetBatchRecallRequest(Guid sessionId)
		{
			return XmlFileGeneratorHelpers.BatchRecallRequestGenerator.GetBatchRecallDataFromTable(sessionId);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="batchRecallRows"></param>
		/// <returns></returns>
		public static XDocument GetBatchRecallRequest(IEnumerable<Model.DataSet1.xBatchRecallRow> batchRecallRows)
		{
			return XmlFileGeneratorHelpers.BatchRecallRequestGenerator.GetBatchRecallDataFromTable(batchRecallRows);
		} 
		#endregion
		public static XDocument GetMasterReportRequest()
		{
			return XmlFileGeneratorHelpers.MasterReportRequestGenerator.GetMasterReportDataFromTable();
		}
    }
}
