﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EmvoInterface.Test
{
	[TestClass]
	public class XmlFileGeneratorTest
	{
		[TestMethod]
		public void TestGetMasterDataRequest()
		{

		}
		[TestMethod]
		public void TestGetPackDataVerificationRequest()
		{

		}
		[TestMethod]
		public void TestGetPackCreateRequest()
		{
			Guid sessionId = Guid.Parse("849B9A4A-30D9-4417-A586-D3E66448E6B2");
			var xPackCreate = XmlFileGenerator.GetPackCreateRequest(sessionId);
			var xStr = xPackCreate.ToString();
		}
		[TestMethod]
		public void TestGetPackUpdateRequest()
		{

		}
		[TestMethod]
		public void TestGetBatchUpdateRequest()
		{

		}
		[TestMethod]
		public void TestGetBatchRecallRequest()
		{

		}
		[TestMethod]
		public void TestGetMasterReportRequest()
		{

		}

	}
}
