# EmvoInterface

## Erzeugen von XML-Dateien

Der folgende Code-Ausschnitt zeigt beispielhaft, wie mithilfe von EmvoInterface eine XML-Datei für den Pack Create Request erzeugt werden und auf der Festplatte gespeichert werden kann.
### C#:
```chsarp
XDocument xdoc = EmvoInterface.XmlFileGenerator.GetPackCreateRequest(sessionId);
xdoc.Save("C:/path/to/create_pack_xml_file.xml");
```
### VB.NET
```vbnet
Dim xdoc As XDocument = EmvoInterface.XmlFileGenerator.GetPackCreateRequest(sessionId)
xdoc.Save("C:/path/to/create_pack_xml_file.xml")
```	